// mailerclient.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const MicroserviceClient = require('@fuzzy-ai/microservice-client')

class MailerClient extends MicroserviceClient {
  send (template, to, subject, data, callback) {
    return this.post(`/${template}`, {to, subject, data}, callback)
  }

  version (callback) {
    return this.get('/version', callback)
  }
}

module.exports = MailerClient
