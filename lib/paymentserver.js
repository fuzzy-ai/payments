// paymentserver.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')
const moment = require('moment')
const _ = require('lodash')
const async = require('async')
const debug = require('debug')('payments')

const Microservice = require('@fuzzy-ai/microservice')

const version = require('./version')
const Customer = require('./customer')
const Invoice = require('./invoice')
const HTTPError = require('./httperror')

const AuthClient = require('./authclient')
const MailerClient = require('./mailerclient')

class PaymentServer extends Microservice {
  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)
    cfg.stripeKey = env['STRIPE_KEY']
    cfg.authServer = env['PAYMENTS_AUTH_SERVER']
    cfg.authKey = env['PAYMENTS_AUTH_KEY']
    cfg.mailerServer = env['PAYMENTS_MAILER_SERVER']
    cfg.mailerKey = env['PAYMENTS_MAILER_KEY']
    if (cfg.stripeKey) {
      this.stripe = require('stripe')(cfg.stripeKey)
    }

    return cfg
  }

  setupExpress () {
    const exp = super.setupExpress()
    exp.stripe = this.stripe
    exp.auth = new AuthClient(this.config.authServer, this.config.authKey)
    exp.mailer = new MailerClient({
      root: this.config.mailerServer,
      key: this.config.mailerKey
    })
    return exp
  }

  setupParams (exp) {
    exp.param('customerID', this._customerIdParam)
    exp.param('invoiceID', this._invoiceIdParam)
  }

  setupRoutes (exp) {
    exp.post('/customers', this._addNewCustomer.bind(this))
    exp.get('/customers/:customerID', this._getCustomerById.bind(this))
    exp.put('/customers/:customerID', this._updateCustomer.bind(this))
    exp.post('/customers/:customerID/refresh', this._refreshCustomer.bind(this))
    exp.get('/customers/:customerID/invoices', this._getInvoices.bind(this))

    exp.post('/invoices', this._addNewInvoice.bind(this))
    exp.post('/invoices/paid', this._invoicePaid.bind(this))
    exp.get('/invoices/:invoiceID', this._getInvoiceById.bind(this))
    exp.put('/invoices/:invoiceID', this._updateInvoice.bind(this))

    exp.get('/live', this.dontLog, this._getLive.bind(this))
    exp.get('/ready', this.dontLog, this._getReady.bind(this))
    exp.get('/version', this._getVersion.bind(this))
  }

  startCustom (callback) {
    this._checkInvoices()
    this.interval = setInterval(this._checkInvoices.bind(this), 24 * 60 * 60 * 1000)
    callback(null)
  }

  stopCustom (callback) {
    clearInterval(this.interval)
    callback(null)
  }

  getSchema () {
    return {
      'customer': Customer.schema,
      'invoice': Invoice.schema
    }
  }

  _customerIdParam (req, res, next, id) {
    return Customer.get(id, (err, customer) => {
      if (err) {
        return next(err)
      } else {
        req.customer = customer
        return next()
      }
    })
  }

  _invoiceIdParam (req, res, next, id) {
    return Invoice.get(id, (err, invoice) => {
      if (err) {
        return next(err)
      } else {
        req.invoice = invoice
        return next()
      }
    })
  }

  _addNewCustomer (req, res, next) {
    const { stripe } = req.app
    let data = req.body
    const trialEnd = moment().endOf('month').format('X')

    debug(data)

    // scope is hard
    let customer = null
    let user = null

    if (!data) {
      debug('Failing with no data')
      return next(new Error('No body'))
    }

    debug('starting the waterfall')

    async.waterfall([
      (callback) => {
        assert(_.isFunction(callback))
        const props = {
          email: data.email,
          source: data.source
        }
        debug('creating customer on stripe')
        debug(props)
        stripe.customers.create(props, callback)
      },
      (result, callback) => {
        assert(_.isObject(result))
        assert(_.isFunction(callback))
        customer = result
        customer.userID = data.userID
        debug('creating customer in db')
        Customer.create(customer, callback)
      },
      (customer, callback) => {
        assert(_.isObject(customer))
        assert(_.isFunction(callback))
        const { auth } = req.app
        const props = {
          customerID: customer.id,
          plan: data.plan
        }
        debug('Updating user data')
        auth.updateUser(data.userID, props, callback)
      },
      (response, callback) => {
        assert(_.isObject(response))
        assert(_.isFunction(callback))

        user = response.user // eslint-disable-line

        debug('Getting plan from stripe')
        stripe.plans.retrieve(data.plan)
          .then(result => callback(null, result))
          .catch(err => callback(err))
      },
      (plan, callback) => {
        assert(_.isObject(plan))
        assert(_.isFunction(callback))
        const days = moment().daysInMonth()
        const today = moment().date()
        const amount = Math.floor(((days - today) / days) * plan.amount)
        const props = {
          customer: customer.id,
          amount,
          currency: 'usd',
          description: 'First month (pro-rated)'
        }
        debug('creating pro-rated invoice item')
        stripe.invoiceItems.create(props)
          .then(result => callback(null, result))
          .catch(err => callback(err))
      },
      (invoiceItem, callback) => {
        assert(_.isObject(invoiceItem))
        assert(_.isFunction(callback))
        if (data.coupon) {
          debug('getting a coupon')
          stripe.coupons.retrieve(data.coupon)
            .then(result => callback(null, result))
            .catch(err => callback(err))
        } else {
          debug('skipping the coupon part')
          callback(null, null)
        }
      },
      (result, callback) => {
        assert(_.isObject(result) || _.isNull(result))
        assert(_.isFunction(callback))
        data = {
          customer: customer.id,
          plan: data.plan,
          trialEnd
        }
        if (result) {
          data.coupon = result.id
        }
        debug('creating a new subscription')
        stripe.subscriptions.create(data)
          .then(result => callback(null, result))
          .catch(err => callback(err))
      }],
    (err, subscription) => {
      assert(_.isObject(err) || _.isNull(err))
      assert(_.isObject(subscription))
      if (err) {
        debug('error')
        next(err)
      } else {
        debug('success, returning subscription data')
        res.json({
          status: 'OK',
          customer,
          user
        })
      }
    })
  }

  _getCustomerById (req, res, next) {
    return res.json({
      status: 'OK',
      customer: req.customer
    })
  }

  _updateCustomer (req, res, next) {
    const { stripe } = req.app
    const data = req.body
    // Check current subscription
    if (__guard__(req.customer.subscriptions != null ? req.customer.subscriptions.data[0] : undefined, x => x.trialEnd)) {
      data.prorate = false
      data.trialEnd = __guard__(req.customer.subscriptions != null ? req.customer.subscriptions.data[0] : undefined, x1 => x1.trialEnd)
    } else {
      data.prorate = true
    }
    stripe.customers.update(req.customer.id, data, (err, customer) => {
      if (err) {
        return next(err)
      } else {
        return req.customer.update(customer, (err, customer) => {
          if (err) {
            return next(err)
          } else {
            if (data.plan) {
              // update auth with new plan
              const { auth } = req.app
              const props = { plan: data.plan }
              return auth.updateUser(customer.userID, props, (err, response) => {
                if (err) {
                  return next(err)
                } else {
                  return res.json({
                    status: 'OK',
                    customer,
                    user: response.user
                  })
                }
              })
            } else {
              return res.json({
                status: 'OK',
                customer
              })
            }
          }
        })
      }
    })
  }

  _refreshCustomer (req, res, next) {
    const { stripe } = req.app
    return stripe.customers.retrieve(req.customer.id, (err, customer) => {
      if (err) {
        return next(err)
      } else {
        return req.customer.update(customer, (err, customer) => {
          if (err) {
            return next(err)
          } else {
            return res.json({
              status: 'OK',
              customer
            })
          }
        })
      }
    })
  }

  _getInvoices (req, res, next) {
    return Invoice.search({customer: req.customer.id}, (err, invoices) => {
      if (err) {
        return next(err)
      } else {
        return res.json({
          status: 'OK',
          invoices
        })
      }
    })
  }

  _addNewInvoice (req, res, next) {
    const data = req.body
    return Invoice.create(data, (err, invoice) => {
      if (err) {
        return next(err)
      } else {
        return res.json({
          status: 'OK',
          invoice
        })
      }
    })
  }

  _invoicePaid (req, res, next) {
    const invoice = req.body
    const { mailer } = req.app
    return Customer.get(invoice.customer, (err, customer) => {
      if (err) {
        return req.log.error({err}, 'Error sending invoice email')
      } else {
        setImmediate(() => {
          const { email } = customer
          const subject = 'Invoice from fuzzy.ai'

          const sub = _.find(invoice.lines.data, ['type', 'subscription'])
          const props = {
            plan: sub.plan.name,
            total: `$${invoice.total / 100} ${invoice.currency}`,
            date: moment.unix(invoice.date).format('MMMM D, YYYY'),
            charge: invoice.charge
          }
          return mailer.send('invoice-paid', email, subject, props, (err) => {
            if (err) {
              const logdata = {err, invoice}
              return req.log.error(logdata, 'Error sending invoice email')
            }
          })
        })
        return res.json({
          status: 'OK',
          invoice
        })
      }
    })
  }

  _getInvoiceById (req, res, next) {
    return res.json({
      status: 'OK',
      invoice: req.invoice
    })
  }

  _updateInvoice (req, res, next) {
    const data = req.body
    return req.invoice.update(data, (err, invoice) => {
      if (err) {
        return next(err)
      } else {
        return res.json({
          status: 'OK',
          invoice
        })
      }
    })
  }

  _checkInvoices () {
    debug('checking invoices')
    if (!this.express.stripe) {
      debug('Stripe not setup')
      return
    }

    const { stripe } = this.express

    const getInvoicesForCustomer = function (customer) {
      debug(`Getting invoices for ${customer.id}`)
      return stripe.invoices.list(
        {customer: customer.id}
        , (err, invoices) => {
          if (err) {
            return debug('Error fetching invoices ')
          } else {
            return _.forEach(invoices.data, (invoice) => {
              debug(`Found invoice ${invoice.id}`)
              return Invoice.get(invoice.id, (err, result) => {
                if (!err) {
                  return
                }
                if (err.name === 'NoSuchThingError') {
                  debug('Creating invoice')
                  return Invoice.create(invoice, (err, invoice) => {
                    if (err) {
                      return debug('Invoice save error')
                    }
                  })
                }
              })
            })
          }
        })
    }

    return Customer.scan(getInvoicesForCustomer, (err) => {
      if (err) {
        return debug('Customer scan error')
      }
    })
  }

  _getLive (req, res, next) {
    return res.json({status: 'OK'})
  }

  _getReady (req, res, next) {
    if ((this.db == null)) {
      return next(HTTPError('Database not connected', 500))
    }

    return async.parallel([
      callback => {
        // Check that we can contact the database
        return this.db.save('server-ready', 'payments', 1, callback)
      },
      callback => {
        return req.app.mailer.version(callback)
      },
      callback => {
        return req.app.auth.version(callback)
      },
      callback => {
        // Check that we can reach Stripe
        // FIXME: this seems like a heavyweight API call to make; maybe
        // there's something slimmer?
        return req.app.stripe.balance.retrieve(callback)
      }

    ], (err) => {
      if (err != null) {
        debug(`${err.name}: ${err.message} in /ready`)
        return next(err)
      } else {
        return res.json({status: 'OK'})
      }
    })
  }

  _getVersion (req, res, next) {
    return res.json({name: 'payments', version})
  }
}

module.exports = PaymentServer

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
