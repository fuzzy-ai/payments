// customer.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')

const Customer = db.DatabankObject.subClass('customer')

Customer.schema = {
  pkey: 'id',
  fields: [
    'email',
    'userID',
    'default_source',
    'sources',
    'subscriptions',
    'delinquent',
    'createdAt',
    'updatedAt'
  ],
  indices: ['email']
}

Customer.beforeCreate = function (props, callback) {
  const required = ['id', 'email', 'default_source', 'sources', 'subscriptions']
  for (const prop of Array.from(required)) {
    if (!props[prop]) {
      callback(new Error(`${prop} property required`))
    }
  }

  props.createdAt = (props.updatedAt = (new Date()).toISOString())
  callback(null, props)
}

Customer.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = (new Date()).toISOString()
  callback(null, props)
}

module.exports = Customer
