// invoice.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')

const Invoice = db.DatabankObject.subClass('invoice')

Invoice.schema = {
  pkey: 'id',
  fields: [
    'customer',
    'createdAt',
    'updatedAt'
  ],
  indices: ['customer']
}

Invoice.beforeCreate = function (props, callback) {
  const required = ['id']
  for (const prop of Array.from(required)) {
    if (!props[prop]) {
      callback(new Error(`${prop} property required`))
    }
  }

  props.createdAt = (props.updatedAt = (new Date()).toISOString())
  callback(null, props)
}

Invoice.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = (new Date()).toISOString()
  callback(null, props)
}

module.exports = Invoice
