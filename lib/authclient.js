// authclient.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const _ = require('lodash')
const request = require('request')

class ClientError extends Error {
  constructor (message, statusCode) {
    super(message)
    this.statusCode = statusCode
    this.name = 'ClientError'
    Error.captureStackTrace(this, ClientError)
  }
}

class ServerError extends Error {
  constructor (message, statusCode) {
    super(message)
    this.statusCode = statusCode
    this.name = 'ServerError'
    Error.captureStackTrace(this, ServerError)
  }
}

class AuthClient {
  constructor (authServer, authKey, caCert = null) {
    this.authServer = authServer
    this.authKey = authKey
    this.caCert = caCert
    const full = rel => {
      return this.authServer + rel
    }

    const handle = verb => {
      return (rel, body, token, callback) => {
        if (_.isFunction(body)) { // rel, callback
          callback = body
          token = this.authKey
          body = null
        } else if (_.isFunction(token)) { // rel, body, callback
          callback = token
          token = this.authKey
        }

        const options = {
          method: verb,
          url: full(rel),
          json: body || true,
          headers: {
            authorization: `Bearer ${token}`
          }
        }

        if (this.caCert) {
          options.agentOptions =
            {ca: this.caCert}
        }

        return request(options, (err, response, body) => {
          if (err) {
            callback(err)
          } else if ((response.statusCode >= 400) && (response.statusCode < 500)) {
            callback(new ClientError(body.message || body, response.statusCode))
          } else if ((response.statusCode >= 500) && (response.statusCode < 600)) {
            callback(new ServerError(body.message || body, response.statusCode))
          } else {
            callback(null, body)
          }
        })
      }
    }

    const get = handle('GET')
    const put = handle('PUT')

    this.updateUser = (userID, props, callback) => put(`/user/${userID}`, props, callback)

    this.version = callback => get('/version', callback)
  }
}

module.exports = AuthClient
