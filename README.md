# payments
Payments endpoint for fuzzy.ai

## Environment Variables

* `STRIPE_KEY`: The Stripe secret key to use (for the Stripe API).
* `PAYMENTS_AUTH_SERVER`: URL of the auth server (to update users)
* `PAYMENTS_AUTH_KEY`: API Key for auth server
* `PAYMENTS_MAILER_SERVER`: URL of the mailer microservice server to use.
* `PAYMENTS_MAILER_KEY`: API key to use to access the mailer microservice.

## Endpoints

* `POST /customers`: Create a new customer given a source token from [Stripe.js](https://stripe.com/docs/stripe.js)

## Authorization

Uses standard fuzzy.ai-microservice `APP_KEY` authorization for `/customers`.
