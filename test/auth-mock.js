// auth-mock.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const http = require('http')
const events = require('events')

const JSON_TYPE = 'application/json; charset=utf-8'

class AuthServerMock extends events.EventEmitter {
  constructor (code) {
    super()
    const server = http.createServer((request, response) => {
      let body = ''
      const respond = function (code, body) {
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      return request.on('end', () => {
        const auth = request.headers.authorization
        if (!auth || !auth.match(new RegExp(`^\\s*Bearer\\s+${code}\\s*$`))) {
          respond(403, {status: 'error', message: 'No access'})
        }
        if (request.method === 'PUT') {
          // self.emit "template", name, body
          return respond(200, {status: 'OK', user: JSON.parse(body)})
        } else if ((request.method === 'GET') && (request.url === '/version')) {
          return respond(200, {version: '0.0.0'})
        } else {
          // If we get here, no route found
          return respond(404, {status: 'error', message: `Cannot ${request.method} ${request.url}`})
        }
      })
    })

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(1517)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = AuthServerMock
