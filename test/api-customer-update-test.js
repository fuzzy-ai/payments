// api-customer-update-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')

const StripeServerMock = require('./stripe-mock')
const AuthServerMock = require('./auth-mock')

const env = require('./config')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('PUT /customers/:customerId')
  .addBatch({
    'When we start a mock auth server': {
      topic () {
        const { callback } = this
        const auth = new AuthServerMock('unittestkey')
        auth.start(err => callback(err, auth))
        return undefined
      },
      'it works' (err, auth) {
        assert.ifError(err)
        assert.isObject(auth)
      },
      'teardown' (auth) {
        auth.stop(this.callback)
      },
      'When we start a mock Stripe server': {
        topic () {
          const { callback } = this
          const mock = new StripeServerMock()
          mock.start(err => callback(err, mock))
          return undefined
        },
        'it works' (err, mock) {
          assert.ifError(err)
          assert.isObject(mock)
        },
        'teardown' (mock) {
          const { callback } = this
          mock.stop(err => callback(err))
          return undefined
        },
        'and we start a PaymentServer': {
          topic (mock) {
            const { callback } = this
            try {
              const PaymentServer = require('../lib/paymentserver')
              const server = new PaymentServer(env)
              server.express.stripe = require('stripe')('test_00000000000')
              server.express.stripe.setHost('localhost', 1516, 'http')
              server.start((err) => {
                if (err) {
                  callback(err, null)
                } else {
                  callback(null, server)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'teardown' (server) {
            const { callback } = this
            server.stop(err => callback(err))
            return undefined
          },
          'and we post customer data': {
            topic (server, mock) {
              const { callback } = this
              return async.parallel([
                callback =>
                  mock.once('create_customer', (req, res) => callback(null)),
                function (callback) {
                  const options = {
                    url: 'http://localhost:2342/customers',
                    json: {
                      email: 'fakeuser@example.com',
                      source: 'tok_00000000000000',
                      plan: 'unit_test',
                      userID: '123456'
                    }
                  }
                  return request.post(options, (err, response, body) => {
                    if (err) {
                      callback(err)
                    } else if (response.statusCode !== 200) {
                      callback(new Error(`Bad status code ${response.statusCode}: ${(body != null ? body.message : undefined)}`))
                    } else {
                      callback(null, body)
                    }
                  })
                }
              ], (err, results) => {
                if (err) {
                  callback(err)
                } else {
                  callback(null, results[1])
                }
              })
            },
            'it works' (err, response) {
              assert.ifError(err)
              assert.equal(response.status, 'OK')
              assert.isObject(response.customer)
              assert.isObject(response.user)
            },
            'and we update the customer': {
              topic (response, server, mock) {
                const { callback } = this
                const { customer } = response
                return async.parallel([
                  callback =>
                    mock.once('update_customer', (req, res) => callback(null)),
                  function (callback) {
                    const options = {
                      url: `http://localhost:2342/customers/${customer.id}`,
                      headers: {
                        'Authorization': `Bearer ${env.APP_KEY_UNIT_TEST}`
                      },
                      json: {
                        source: 'tok_00000000000001'
                      }
                    }
                    return request.put(options, (err, response, body) => {
                      if (err) {
                        callback(err)
                      } else if (response.statusCode !== 200) {
                        callback(new Error(`Bad status code ${response.statusCode}: ${(body != null ? body.message : undefined)}`))
                      } else {
                        callback(null, body)
                      }
                    })
                  }
                ], (err, results) => {
                  if (err) {
                    callback(err)
                  } else {
                    callback(null, results[1])
                  }
                })
              },
              'it works' (err, response) {
                assert.ifError(err)
                assert.isString(response.status, 'OK')
                assert.isObject(response.customer)
              }
            }
          }
        }
      }
    }}).export(module)
