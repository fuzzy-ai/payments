// api-batch.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const _ = require('lodash')

const StripeServerMock = require('./stripe-mock')
const AuthServerMock = require('./auth-mock')
const MailerServerMock = require('./mailer-mock')

const env = require('./config')

const base = {
  'When we start a mock auth server': {
    topic () {
      const { callback } = this
      const auth = new AuthServerMock('unittestkey')
      auth.start(err => callback(err, auth))
      return undefined
    },
    'it works' (err, auth) {
      assert.ifError(err)
      assert.isObject(auth)
    },
    'teardown' (auth) {
      auth.stop(this.callback)
    },
    'When we start a mock Stripe server': {
      topic () {
        const { callback } = this
        const mock = new StripeServerMock()
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        const { callback } = this
        mock.stop(err => callback(err))
        return undefined
      },
      'When we start a mock mailer server': {
        topic () {
          const { callback } = this
          const mock = new MailerServerMock('testappkey')
          mock.start(err => callback(err, mock))
          return undefined
        },
        'it works' (err, mock) {
          assert.ifError(err)
          assert.isObject(mock)
        },
        'teardown' (mock) {
          const { callback } = this
          mock.stop(err => callback(err))
          return undefined
        },
        'and we start a PaymentServer': {
          topic (mock) {
            const { callback } = this
            try {
              const PaymentServer = require('../lib/paymentserver')
              const server = new PaymentServer(env)
              server.express.stripe = require('stripe')('test_00000000000')
              server.express.stripe.setHost('localhost', 1516, 'http')
              server.start((err) => {
                if (err) {
                  callback(err, null)
                } else {
                  callback(null, server)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'teardown' (server) {
            const { callback } = this
            server.stop(err => callback(err))
            return undefined
          }
        }
      }
    }
  }
}

const apiBatch = function (rest) {
  const batch = _.cloneDeep(base)
  _.extend(batch['When we start a mock auth server']['When we start a mock Stripe server']['When we start a mock mailer server']['and we start a PaymentServer'], rest)
  return batch
}

module.exports = apiBatch
