// api-invoice-update-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const StripeServerMock = require('./stripe-mock')
const AuthServerMock = require('./auth-mock')

const env = require('./config')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('PUT /invoices/:invoiceId')
  .addBatch({
    'When we start a mock auth server': {
      topic () {
        const { callback } = this
        const auth = new AuthServerMock('unittestkey')
        auth.start(err => callback(err, auth))
        return undefined
      },
      'it works' (err, auth) {
        assert.ifError(err)
        assert.isObject(auth)
      },
      'teardown' (auth) {
        auth.stop(this.callback)
      },
      'When we start a mock Stripe server': {
        topic () {
          const { callback } = this
          const mock = new StripeServerMock()
          mock.start(err => callback(err, mock))
          return undefined
        },
        'it works' (err, mock) {
          assert.ifError(err)
          assert.isObject(mock)
        },
        'teardown' (mock) {
          const { callback } = this
          mock.stop(err => callback(err))
          return undefined
        },
        'and we start a PaymentServer': {
          topic (mock) {
            const { callback } = this
            try {
              const PaymentServer = require('../lib/paymentserver')
              const server = new PaymentServer(env)
              server.express.stripe = require('stripe')('test_00000000000')
              server.express.stripe.setHost('localhost', 1516, 'http')
              server.start((err) => {
                if (err) {
                  callback(err, null)
                } else {
                  callback(null, server)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'teardown' (server) {
            const { callback } = this
            server.stop(err => callback(err))
            return undefined
          },
          'and we post Invoice data': {
            topic (server, mock) {
              const { callback } = this
              const options = {
                url: 'http://localhost:2342/invoices',
                json: {
                  id: 'invoice_00000000000001'
                }
              }
              request.post(options, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 200) {
                  callback(new Error(`Bad status code ${response.statusCode}: ${(body != null ? body.message : undefined)}`))
                } else {
                  callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, response) {
              assert.ifError(err)
              assert.isString(response.status, 'OK')
              assert.isObject(response.invoice)
            },
            'and we update the invoice': {
              topic (result, server, mock) {
                const { callback } = this
                const options = {
                  url: `http://localhost:2342/invoices/${result.invoice.id}`,
                  json: {
                    foo: 'bar'
                  }
                }
                request.put(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 200) {
                    callback(new Error(`Bad status code ${response.statusCode}: ${(body != null ? body.message : undefined)}`))
                  } else {
                    callback(null, body)
                  }
                })
                return undefined
              },
              'it works' (err, response) {
                assert.ifError(err)
                assert.isString(response.status, 'OK')
                assert.isObject(response.invoice)
              }
            }
          }
        }
      }
    }}).export(module)
