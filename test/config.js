// config.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const env = {
  PORT: '2342',
  DRIVER: 'memory',
  PARAMS: '{}',
  LOG_FILE: '/dev/null',
  APP_KEY_UNIT_TEST: 'verdipuckrinkbanwilt',
  PAYMENTS_AUTH_SERVER: 'http://localhost:1517',
  PAYMENTS_AUTH_KEY: 'unittestkey',
  PAYMENTS_MAILER_SERVER: 'http://localhost:1518',
  PAYMENTS_MAILER_KEY: 'testappkey'
}

module.exports = env
