// stripe-mock.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const events = require('events')
const fs = require('fs')
const http = require('http')
const path = require('path')

const JSON_TYPE = 'application/json; charset=utf-8'

class StripeServerMock extends events.EventEmitter {
  constructor () {
    super()
    const self = this
    const server = http.createServer((request, response) => {
      let body = '' // eslint-disable-line no-unused-vars
      const respond = function (code, body) {
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      return request.on('end', () => {
        let data
        const [version, resource, id] = Array.from(request.url.slice(1).split('/')) // eslint-disable-line no-unused-vars
        if (request.method === 'GET') {
          if (resource === 'plans') {
            self.emit('retrieve_plan')
            data = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/plan-data.json')))
            respond(200, data)
          }
          if (resource === 'invoices') {
            self.emit('retrieve_invoices')
            respond(200, {data: []})
          }
          if (resource === 'balance') {
            return respond(200, {})
          }
        } else if (request.method === 'POST') {
          if (resource === 'customers') {
            if (id) {
              self.emit('update_customer')
            } else {
              self.emit('create_customer')
            }
            data = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/customer-data.json')))
            respond(200, data)
          } else if (resource === 'invoiceitems') {
            self.emit('create_invoiceitems')
            data = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/invoiceitems-data.json')))
            respond(200, data)
          } else if (resource === 'subscriptions') {
            self.emit('create_subscriptions')
            data = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/subscriptions-data.json')))
            respond(200, data)
          }
          return respond(200, {status: 'OK'})
        } else {
          // If we get here, no route found
          return respond(404, {status: 'error', message: `Cannot ${request.method} ${request.url}`})
        }
      })
    })

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(1516)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = StripeServerMock
