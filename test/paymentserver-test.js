// paymentserver-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

vows
  .describe('Payments server')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const PaymentServer = require('../lib/paymentserver')
          callback(null, PaymentServer)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, PaymentServer) {
        assert.ifError(err)
      },
      'it is a class' (err, PaymentServer) {
        assert.ifError(err)
        assert.isFunction(PaymentServer)
      },
      'and we instantiate an PaymentServer': {
        topic (PaymentServer) {
          const { callback } = this
          try {
            const env = {
              PORT: 2342,
              DRIVER: 'memory',
              HOSTNAME: 'localhost',
              LOG_FILE: '/dev/null',
              PAYMENTS_MAILER_SERVER: 'http://localhost:1518',
              PAYMENTS_MAILER_KEY: 'test-app-key'
            }
            const server = new PaymentServer(env)
            callback(null, server)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'it is an object' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
        },
        'it has a start() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          assert.isFunction(server.start)
        },
        'it has a stop() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          assert.isFunction(server.stop)
        },
        'and we start the server': {
          topic (server) {
            const { callback } = this
            server.start((err) => {
              if (err) {
                callback(err)
              } else {
                callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          },
          'and we request the version': {
            topic () {
              const { callback } = this
              const url = 'http://localhost:2342/version'
              request.get(url, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 200) {
                  callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  body = JSON.parse(body)
                  callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, version) {
              assert.ifError(err)
            },
            'it looks correct' (err, version) {
              assert.ifError(err)
              assert.include(version, 'version')
              assert.include(version, 'name')
            },
            'and we stop the server': {
              topic (version, server) {
                const { callback } = this
                server.stop((err) => {
                  if (err) {
                    callback(err)
                  } else {
                    callback(null)
                  }
                })
                return undefined
              },
              'it works' (err) {
                assert.ifError(err)
              },
              'and we request the version': {
                topic () {
                  const { callback } = this
                  const url = 'http://localhost:2342/version'
                  request.get(url, (err, response, body) => {
                    if (err) {
                      callback(null)
                    } else {
                      callback(new Error('Unexpected success after server stop'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
