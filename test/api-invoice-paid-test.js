// api-customer-create-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')

const StripeServerMock = require('./stripe-mock')
const AuthServerMock = require('./auth-mock')
const MailerServerMock = require('./mailer-mock')

const env = require('./config')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('POST /invoice/paid')
  .addBatch({
    'When we start a mock auth server': {
      topic () {
        const { callback } = this
        const auth = new AuthServerMock('unittestkey')
        auth.start(err => callback(err, auth))
        return undefined
      },
      'it works' (err, auth) {
        assert.ifError(err)
        assert.isObject(auth)
      },
      'teardown' (auth) {
        auth.stop(this.callback)
      },
      'When we start a mock Stripe server': {
        topic () {
          const { callback } = this
          const mock = new StripeServerMock()
          mock.start(err => callback(err, mock))
          return undefined
        },
        'it works' (err, mock) {
          assert.ifError(err)
          assert.isObject(mock)
        },
        'teardown' (mock) {
          const { callback } = this
          mock.stop(err => callback(err))
          return undefined
        },
        'and we start a PaymentServer': {
          topic (mock) {
            const { callback } = this
            try {
              const PaymentServer = require('../lib/paymentserver')
              const server = new PaymentServer(env)
              server.express.stripe = require('stripe')('test_00000000000')
              server.express.stripe.setHost('localhost', 1516, 'http')
              server.start((err) => {
                if (err) {
                  callback(err, null)
                } else {
                  callback(null, server)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'teardown' (server) {
            const { callback } = this
            server.stop(err => callback(err))
            return undefined
          },
          'and we post customer data': {
            topic (server, mock) {
              const { callback } = this
              return async.parallel([
                callback =>
                  mock.once('create_customer', (req, res) => callback(null)),
                callback =>
                  mock.once('retrieve_plan', (req, res) => callback(null)),
                callback =>
                  mock.once('create_invoiceitems', (req, res) => callback(null)),
                callback =>
                  mock.once('create_subscriptions', (req, res) => callback(null)),
                function (callback) {
                  const options = {
                    url: 'http://localhost:2342/customers',
                    json: {
                      email: 'fakeuser@example.com',
                      source: 'tok_00000000000000',
                      plan: 'unit_test',
                      userID: '123456'
                    }
                  }
                  return request.post(options, (err, response, body) => {
                    if (err) {
                      callback(err)
                    } else if (response.statusCode !== 200) {
                      callback(new Error(`Bad status code ${response.statusCode}: ${(body != null ? body.message : undefined)}`))
                    } else {
                      callback(null, body)
                    }
                  })
                }
              ], (err, results) => {
                if (err) {
                  callback(err)
                } else {
                  callback(null, results[4])
                }
              })
            },
            'it works' (err, response) {
              assert.ifError(err)
              assert.equal(response.status, 'OK')
              assert.isObject(response.customer)
              assert.isObject(response.user)
            },
            'When we start a mock mailer server': {
              topic () {
                const { callback } = this
                const mock = new MailerServerMock('testappkey')
                mock.start(err => callback(err, mock))
                return undefined
              },
              'it works' (err, mock) {
                assert.ifError(err)
                assert.isObject(mock)
              },
              'teardown' (mock) {
                const { callback } = this
                mock.stop(err => callback(err))
                return undefined
              },
              'and we post invoice paid': {
                topic (mailer, response) {
                  const { callback } = this
                  const { customer } = response
                  async.parallel([
                    callback =>
                      mailer.once('template', (name, paramString) => callback(null)),
                    function (callback) {
                      const options = {
                        url: 'http://localhost:2342/invoices/paid',
                        headers: {
                          'Authorization': `Bearer ${env.APP_KEY_UNIT_TEST}`
                        },
                        json: {
                          customer: customer.id,
                          lines: {
                            data: [{
                              type: 'subscription',
                              plan: {
                                name: 'Standard Plan'
                              }
                            }
                            ]
                          },
                          total: 4900,
                          date: new Date().getTime(),
                          charge: 'chg_0000000000001'
                        }
                      }
                      return request.post(options, (err, response, body) => {
                        if (err) {
                          callback(err)
                        } else if (response.statusCode !== 200) {
                          callback(new Error(`Bad status code ${response.statusCode}: ${(body != null ? body.message : undefined)}`))
                        } else {
                          callback(null, body)
                        }
                      })
                    }
                  ], (err, results) => {
                    if (err) {
                      callback(err)
                    } else {
                      callback(null, results[1])
                    }
                  })
                  return undefined
                },
                'it works' (err, response) {
                  assert.ifError(err)
                  assert.isObject(response)
                }
              }
            }
          }
        }
      }
    }}).export(module)
