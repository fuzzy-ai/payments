// api-live-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./apibatch')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('/live endpoint')
  .addBatch(apiBatch({
    'and we get the /live endpoint': {
      topic () {
        request.get('http://localhost:2342/live', this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        assert.equal(response.statusCode, 200)
        assert.isString(body)
        assert.equal(JSON.parse(body).status, 'OK')
      }
    }
  })).export(module)
