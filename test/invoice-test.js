// invoice-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

vows
  .describe('Invoice data type')
  .addBatch({
    'When we load the Invoice module': {
      topic () {
        try {
          const Invoice = require('../lib/invoice')
          this.callback(null, Invoice)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, Invoice) {
        assert.ifError(err)
        assert.isFunction(Invoice)
      },
      'add we set up the database': {
        topic (Invoice) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {invoice: Invoice.schema}
          const db = Databank.get(env.DRIVER, params)
          return db.connect({}, (err) => {
            if (err) {
              callback(err, null)
            } else {
              DatabankObject.bank = db
              callback(null, db)
            }
          })
        },
        'it works' (err, db) {
          assert.ifError(err)
          assert.isObject(db)
        },
        'teardown' (db) {
          const {callback} = this
          if (db) {
            db.disconnect(callback)
          } else {
            callback(null)
          }
        },
        'and we create a new Invoice': {
          topic (db, Invoice) {
            const { callback } = this
            const props = {
              id: 'cus_7nyxlUbEfW5gIN',
              email: 'fakeinvoice@example.com',
              default_source: 'card_17YN012eZvKYlo2CZAa4rCmP',
              sources: {},
              subscriptions: {},
              delinquent: false
            }
            return Invoice.create(props, (err, invoice) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, invoice)
              }
            })
          },
          'it works' (err, invoice) {
            assert.ifError(err)
            assert.isObject(invoice)
            assert.isString(invoice.id)
            assert.isString(invoice.createdAt)
            assert.inDelta(Date.parse(invoice.createdAt), Date.now(), 5000)
            assert.isString(invoice.updatedAt)
            assert.inDelta(Date.parse(invoice.updatedAt), Date.now(), 5000)
          },
          'and we wait 2 seconds': {
            topic (invoice) {
              const { callback } = this
              const wait = () => callback(null)
              setTimeout(wait, 2000)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we update the invoice': {
              topic (invoice) {
                invoice.update({defaultSource: 'card_17YN012eZvKYlo2CZAa40000'}, this.callback)
                return undefined
              },
              'it works' (err, invoice) {
                assert.ifError(err)
                assert.isObject(invoice)
                assert.isString(invoice.id)
                assert.isString(invoice.defaultSource)
                assert.isString(invoice.updatedAt)
                assert.notEqual(invoice.createdAt, invoice.updatedAt)
                assert.inDelta(Date.parse(invoice.updatedAt), Date.now(), 5000)
              }
            }
          }
        }
      }
    }}).export(module)
