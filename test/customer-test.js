// customer-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('Customer data type')
  .addBatch({
    'When we load the Customer module': {
      topic () {
        try {
          const Customer = require('../lib/customer')
          this.callback(null, Customer)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, Customer) {
        assert.ifError(err)
        assert.isFunction(Customer)
      },
      'add we set up the database': {
        topic (Customer) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {customer: Customer.schema}
          const db = Databank.get(env.DRIVER, params)
          return db.connect({}, (err) => {
            if (err) {
              callback(err, null)
            } else {
              DatabankObject.bank = db
              callback(null, db)
            }
          })
        },
        'it works' (err, db) {
          assert.ifError(err)
          assert.isObject(db)
        },
        'teardown' (db) {
          const {callback} = this
          if (db) {
            db.disconnect(callback)
          } else {
            callback(null)
          }
        },
        'and we create a new Customer': {
          topic (db, Customer) {
            const { callback } = this
            const props = {
              id: 'cus_7nyxlUbEfW5gIN',
              email: 'fakecustomer@example.com',
              default_source: 'card_17YN012eZvKYlo2CZAa4rCmP',
              sources: {},
              subscriptions: {},
              delinquent: false
            }
            return Customer.create(props, (err, customer) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, customer)
              }
            })
          },
          'it works' (err, customer) {
            assert.ifError(err)
            assert.isObject(customer)
            assert.isString(customer.id)
            assert.isString(customer.email)
            assert.isString(customer.default_source)
            assert.isObject(customer.sources)
            assert.isObject(customer.subscriptions)
            assert.isBoolean(customer.delinquent)
            assert.isString(customer.createdAt)
            assert.inDelta(Date.parse(customer.createdAt), Date.now(), 5000)
            assert.isString(customer.updatedAt)
            assert.inDelta(Date.parse(customer.updatedAt), Date.now(), 5000)
          },
          'and we wait 2 seconds': {
            topic (customer) {
              const { callback } = this
              const wait = () => callback(null)
              setTimeout(wait, 2000)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we update the customer': {
              topic (customer) {
                customer.update({defaultSource: 'card_17YN012eZvKYlo2CZAa40000'}, this.callback)
                return undefined
              },
              'it works' (err, customer) {
                assert.ifError(err)
                assert.isObject(customer)
                assert.isString(customer.id)
                assert.isString(customer.defaultSource)
                assert.isString(customer.updatedAt)
                assert.notEqual(customer.createdAt, customer.updatedAt)
                assert.inDelta(Date.parse(customer.updatedAt), Date.now(), 5000)
              }
            }
          }
        }
      }
    }}).export(module)
