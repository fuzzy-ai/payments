FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/payments
COPY . /opt/payments

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/payments/bin/dumb-init", "--"]
CMD ["npm", "start"]
